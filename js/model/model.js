export class Product {
  constructor(
    id,
    name,
    price,
    avatar,
    screen,
    frontCamera,
    backCamera,
    type,
    description
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.img = avatar;
    this.screen = screen;
    this.frontCamera = frontCamera;
    this.backCamera = backCamera;
    this.type = type;
    this.desc = description;
  }
}
// bật loading
export let batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
// tắt loading
export let tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};
